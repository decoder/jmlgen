![](logo/JMLGen_Logo_RGB_256x98.png)

# Generate JML annotations in java project

## What is done?

JML annotations are generated, from what can be guessed out of a java project.

Current functions:

- Multiple method calls are parsed, to insert appropriate "@non_null" annotations (a call like myobject.method1().method2().method3() will result in non_null annotations for methods method1() and method2()).
- "@pure" annotations (to denote stateless methods) are inserted in front of public member field getters.
- "@modifies" annotations are inserted in front of methods that affect public member fields.

## Build

```
mvn clean install
```

## Run an example

```
java -jar target/jmlgen-0.0.1-SNAPSHOT.jar src/main/resources/jmlgen.properties
```

Takes as source the example in src/test/java/eu.decoder.sample_jmlgen , and generates output in /tmp/jmlgen (as specified in jmlgen.properties file).

## General usage

Jmlgen takes as input as source a java project (the project root directory, like the one out of a "git clone"), and generates JML in java files located in a specified destination directory.
Which files to parse and where to resolve method calls must be specified in a source path (with possibly multiple directories, separated by colons like for classpaths).

If source and destination directory are the same, java files are overwritten with JML-annotated versions.

This can be specified on the command line, as follows:

```
java -jar target/jmlgen-0.0.1-SNAPSHOT.jar <source-java-project-root> <destination-root> [<sourcepath>]
```

Or using a properties file:

```
java -jar target/jmlgen-0.0.1-SNAPSHOT.jar <properties-file>
```

The following properties must be provided:

```
root: /project/root/directory
destination: /target/directory/for/jmlgen
sourcepath: path1[:path2...:pathN]
```

## Example with real-life project: OW2 sat4j

For testing, go into /tmp directory, then clone sat4j:

```
/tmp$ git clone https://gitlab.ow2.org/sat4j/sat4j.git
```

Create a sat4j.properties file in /tmp, with following content:

```
root: /tmp/sat4j
destination: /tmp/sat4j-JML
sourcepath: org.sat4j.br4cp/src/main/java:org.sat4j.core/src/main/java:org.sat4j.intervalorders/src/main/java:org.sat4j.maxsat/src/main/java:org.sat4j.pb/src/main/java:org.sat4j.sat/src/main/java:org.sat4j.sudoku/src/main/java
```

Then, run JML generator (here, from jmlgen directory):

```
$ java -jar target/jmlgen-0.0.1-SNAPSHOT.jar /tmp/sat4j.properties
```

As specified in properties file, a /tmp/sat4j-JML directory will be created, with the resulting JML files insides.

Note that destination directory will contain only JML-annotated files, other ones are not copied.

Putting back JML into the original project can be done in 2 ways:
- Copy the generated files into the original tree (cp -r /tmp/sat4j-JML/* /tmp/sat4j)
- Specify the same root and destination directories in properties file or on the command line (set "destination" to /tmp/sat4j), then run jmlgen again.

## Known issue for previous JDK/JVM versions

The pom.xml compiles using java version 11, as specified in `maven.compiler.release` property used by Maven compiler plugin:

```
    <properties>
        ...
        <maven.compiler.release>11</maven.compiler.release>
        ...
    </properties>
```

If you need to run jmlc with an older JDK, you may encounter a java.lang.UnsupportedClassVersionError.
There are 2 ways to fix that:
- Upgrade to java 11
- Change release in pom (for example, "8" instead of "11") then rebuild.
                
Also, pom.xml require usage of Maven 3.6.3. As a convenient solution the project include the Maven wrapper configured to provided Maven 3.6.3.

## Credits

The jmlgen tool was initially developed on behalf of the DECODER EU project (https://www.decoder-project.eu).
The DECODER project has received funding from the European Union's Horizon 2020 research and innovation programme under grant agreement number 824231.
