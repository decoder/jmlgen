package eu.decoder.sample_jmlgen;

public class SampleTwo {

	SampleOne one;

	public SampleTwo(SampleOne one) {
		super();
		this.one = one;
	}

	/**
	 * Gets current sample 1
	 * @return a sample 1 instance
	 */
	public SampleOne getOne() {
		return one;
	}

	/**
	 * Sets current sample 1
	 * @param a sample 1 instance
	 */
	public void setOne(SampleOne one) {
		this.one = one;
	}

	public int returnMe(int me) {
		SampleTwo two = new SampleTwo(new SampleOne("Hello", me));
		toString().toString();
		//two.getOne().myself().getAnyInt();
		return two.getOne().myself().myself().getAnyInt();
		//return 0;
	}
	
	public String toString() { return super.toString(); }

}
