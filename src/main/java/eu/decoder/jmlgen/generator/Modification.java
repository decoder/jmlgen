package eu.decoder.jmlgen.generator;

/**
 * Modifications keep track of which JML annotations are inserted, and where.
 * They are intended to be associated with a file.
 * @author Pierre-Yves Gibello - OW2
 *
 */
public class Modification {

	String annotation;
	boolean inline;
	int line, column;
	Object info;

	public Modification(String annotation, boolean inline, int line, int column, Object info) {
		this.annotation = annotation;
		this.inline = inline;
		this.line = line;
		this.column = column;
		this.info = info;
	}

	public boolean isInline() {
		return inline;
	}

	public int getLine() {
		return line;
	}

	public void setLine(int line) {
		this.line = line;
	}
	
	public int getColumn() {
		return column;
	}
	
	public String getAnnotation() {
		return annotation;
	}
	
	public Object getInfo() {
		return info;
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == this) return true;
		if(! (o instanceof Modification)) return false;
		Modification m = (Modification)o;
		return m.inline == inline
			&& m.column == column
			&& m.line == line
			&& (m.annotation == null ? annotation == null : m.annotation.equals(annotation));
	}
	@Override
    public int hashCode() {
        int result = (inline ? 17 : 19);
        result = 31 * result + annotation.hashCode();
        result = 31 * result + line;
        result = 31 * result + column;
        return result;
    }

	public String toString() {
		return "line " + line + ", column " + column + ": " + annotation;
	}
}
