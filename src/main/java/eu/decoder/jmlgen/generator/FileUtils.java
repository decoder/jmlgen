package eu.decoder.jmlgen.generator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

/**
 * File utilities, including modifications management (where JML is inserted)
 * @author Pierre-Yves Gibello - OW2
 *
 */
public class FileUtils {
	
	// A set of modifications can be associated to a file path
	Map<String, Set<Modification>> modifications = new HashMap<>();

	/**
	 * Schedules a modification to be applied to a file
	 * @param path Path to the file
	 * @param modification Modification to schedule
	 */
	public void addModification(String path, Modification modification) {
		Set<Modification> modSet = this.modifications.get(path);
		if(modSet == null) {
			modSet = new HashSet<>();
			this.modifications.put(path, modSet);
		}
		modSet.add(modification);
	}
	
	/**
	 * Retrieves scheduled modifications (set of modifications by file)
	 * @return Set of modifications scheduled
	 */
	public Map<String, Set<Modification>> getModifications() {
		return this.modifications;
	}

	/**
	 * Prints scheduled modifications on given PrintStream
	 * @param out The output PrintStream
	 */
	public void printModifications(PrintStream out) {
		for(String key : this.modifications.keySet()) {
			out.println("Modifications scheduled in " + key + ":");
			Set<Modification> modSet = this.modifications.get(key);
	        for(Modification modification : modSet) {
	        	out.println("\t" + modification);
	        }
		}
	}
	
	/**
	 * Applies scheduled modifications to a file
	 * @param target Target file
	 * @param modifications Modification set
	 * @throws IOException
	 */
	public void applyModifications(File target, Set<Modification> modifications) throws IOException {
		BufferedReader in = null;
		PrintWriter out = null;
		File tmp = null;
		try {
			String line;
			int lineno = 0;
			tmp = File.createTempFile(target.getName(), UUID.randomUUID().toString());
			tmp.deleteOnExit();
			out = new PrintWriter(new FileWriter(tmp));
			in = new BufferedReader(new FileReader(target));
			// 1st apply inline modifications (does not affect line numbers)
			while((line = in.readLine()) != null) {
				lineno++;
				String lineToWrite = line;
				Set<Modification> mods = findModification(modifications, lineno);
				StringBuilder buf = null;
				String endOfLine = null;
				boolean firstInline = true;
				for(Modification mod : mods) {
					if(mod.isInline()) {
						if(buf == null) {
							buf = new StringBuilder(line.substring(0, mod.getColumn()));
							endOfLine = " @*/ " + line.substring(mod.getColumn());
						}
						buf.append((firstInline ? "/*@ " : " ")  + mod.getAnnotation());
						firstInline = false;
					} else {
						// First insert modification
						for(int i=0; i<mod.getColumn(); i++) out.print(" ");
						out.println("/*@ " + mod.getAnnotation() + " @*/");
					}
				}
				if(buf != null) {
					buf.append(endOfLine);
					lineToWrite = buf.toString();
				}
				out.println(lineToWrite);
			}
		} finally {
			if(in != null) try { in.close(); } catch(Exception ignore) { }
			if(out != null) try { out.close(); } catch(Exception ignore) { }
			Files.move(tmp.toPath(),
	    			target.toPath(), StandardCopyOption.REPLACE_EXISTING);
			//if(tmp != null) tmp.delete();
		}
	}
	
	/**
	 * Retrieves modifications to apply to a given line
	 * @param modifications A modification set
	 * @param lineno The line number
	 * @return The set of modifications to apply to specified line
	 */
	private Set<Modification> findModification(Set<Modification> modifications, int lineno) {
		Set<Modification> result = new HashSet<>();
		for(Modification modification : modifications) {
			if(modification.getLine() == lineno) {
				result.add(modification);
			}
		}
		return result;
	}

	/**
	 * Copies file, defined by relative path, between directories
	 * @param src Source directory
	 * @param dest Destination directory
	 * @param relativePath Relative path to the file to copy
	 */
	public static File copyFile(File src, File dest, String relativePath) throws IOException {
		File source = new File(concatPath(src.getAbsolutePath(), relativePath));
		if(! source.exists()) throw new IOException(source.getAbsolutePath() + " not found!");
		else if(source.isDirectory()) throw new IOException(source.getAbsolutePath() + " should be a plain file!");

		if(! dest.exists()){
            dest.mkdirs();
        } else if(! dest.isDirectory()) {
        	throw new IOException(dest.getAbsolutePath() + " should be a directory");
        }
		
		File target = new File(concatPath(dest.getAbsolutePath(), relativePath));
		target.getParentFile().mkdirs();
		Files.copy(source.toPath(),
    			target.toPath(), StandardCopyOption.REPLACE_EXISTING);
		return target;
	}

	/**
	 * Recursively copies java files to destination
	 * @param src source file or directory
	 * @param dest destination file or directory
	 * @throws IOException
	 */
	public static void duplicateJavaTree(File src, File dest) throws IOException {
		if(src.isDirectory()) {
	        if(! dest.exists()){
	            dest.mkdirs();
	        } else if(! dest.isDirectory()) {
	        	throw new IOException(dest.getAbsolutePath() + " should be a directory");
	        }

	        String files[] = src.list();

	        for (String file : files) {
	            File srcFile = new File(src, file);
	            File destFile = new File(dest, file);

	            duplicateJavaTree(srcFile, destFile);
	            // Delete empty directories
	            if(destFile.isDirectory() && ! Files.list(destFile.toPath()).findFirst().isPresent()) {
	            	destFile.delete();
	            }
	        }

	    } else {
	    	if(dest.exists() && dest.isDirectory()) {
	    		throw new IOException(dest.getAbsolutePath() + " should be a plain file");
	    	}
	    	if(src.getName().endsWith(".java")) {
	    		Files.copy(src.toPath(),
	    			dest.getAbsoluteFile().toPath(), StandardCopyOption.REPLACE_EXISTING);
	    	}
	    }
	}

	/**
	 * Concatenates file path to extend it
	 * @param path Original path
	 * @param extend Extension to go deeper
	 * @return The concatenated path
	 */
	public static String concatPath(String path, String extend) {
		extend = (extend == null ? "" : extend.trim());
		if(path != null) {
			path = path.trim();
			if(extend.startsWith(File.separator)) {
				extend = (extend.length() > 1 ? extend.substring(1) : "");
			}
			return path + (path.endsWith(File.separator) ? "" : File.separator) + extend;
		} else return null;
	}
}

