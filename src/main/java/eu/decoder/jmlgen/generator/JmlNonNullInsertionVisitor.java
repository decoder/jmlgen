package eu.decoder.jmlgen.generator;

import java.io.File;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.expr.MethodCallExpr;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;
import com.github.javaparser.resolution.declarations.ResolvedMethodDeclaration;

/**
 * Visitor class to insert JML @non_null
 * @author Pierre-Yves Gibello - OW2
 *
 */
public class JmlNonNullInsertionVisitor extends VoidVisitorAdapter<Void> {

	JmlGenerator generator;
	FileUtils fileUtils;
	File destination;

	public JmlNonNullInsertionVisitor(JmlGenerator generator, FileUtils fileUtils, File destination) {
		super();
		this.generator = generator;
		this.fileUtils = fileUtils;
		this.destination = destination;
	}
	
	@Override
	public void visit(MethodCallExpr mc, Void arg) {
		super.visit(mc, arg);

		// Add @non_null JML annotations when at risk of NPE
		// When multiple calls like obj.a().b().c(), AST looks like this:
		// c
		// |--b
		//    |--a
		if(mc.getChildNodes() != null && mc.getChildNodes().get(0) instanceof MethodCallExpr) {
            try {
                    ResolvedMethodDeclaration resolvedDeclaration = ((MethodCallExpr)mc.getChildNodes().get(0)).resolve();
                    if(! resolvedDeclaration.isAbstract()) { // At least never insert in interfaces

                    	// Prepare modification
                    	Modification modification = ASTUtils.locateInlineAnnotation(resolvedDeclaration.toAst().get(), "non_null");
                    	if(modification != null) {
                    		this.fileUtils.addModification(
                    			this.generator.getCUPath((CompilationUnit)modification.getInfo()),
                    			modification);
                    	}
 
                    }
            } catch(Exception e) {
                    // ignore
                    //if(! e.toString().contains("\'java.")
                    //&& ! e.toString().contains("java.lang.")
                    //&& ! e.toString().contains("\'Math.")
                    //&& ! e.toString().contains("\'System.")
                    //&& ! e.toString().contains("\'Logger."))
                    //System.err.println("Warning: unresolved method " + mc.getName() + ": " + e);
            }
		}
	}

}
