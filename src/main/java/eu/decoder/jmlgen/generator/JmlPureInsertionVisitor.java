package eu.decoder.jmlgen.generator;

import java.io.File;
import java.util.List;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;

/**
 * Visitor class to insert JML @pure
 * @author Pierre-Yves Gibello - OW2
 *
 */
public class JmlPureInsertionVisitor extends VoidVisitorAdapter<Void> {

	JmlGenerator generator;
	FileUtils fileUtils;
	File destination;

	public JmlPureInsertionVisitor(JmlGenerator generator, FileUtils fileUtils, File destination) {
		super();
		this.generator = generator;
		this.fileUtils = fileUtils;
		this.destination = destination;
	}
	
	@Override
	public void visit(MethodDeclaration declaration,  Void arg) {
		super.visit(declaration, arg);

		if(! declaration.isAbstract()) { // At least never insert in interfaces
			try {
				// Add @pure JML annotation to getters
				if(ASTUtils.isGetter(declaration)) {
					// Prepare modification
					Modification modification = ASTUtils.locateInlineAnnotation(declaration, "pure");
					if(modification != null) {
						this.fileUtils.addModification(
								this.generator.getCUPath((CompilationUnit)modification.getInfo()),
								modification);
					}
				} else {
					// Add @modified JML annotation to methods that set public fields
					List<String> fields = ASTUtils.getAssignedPublicFields(declaration);
					for(String field : fields) {
						// Prepare modification
						Modification modification = ASTUtils.locateAnnotation(declaration, "modifies this." + field + ";");
						if(modification != null) {
							this.fileUtils.addModification(
									this.generator.getCUPath((CompilationUnit)modification.getInfo()),
									modification);
						}
					}
				}
			} catch(Exception e) {
				// ignore
			}
		}
	}
}