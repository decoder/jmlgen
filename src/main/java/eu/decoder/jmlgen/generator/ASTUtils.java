package eu.decoder.jmlgen.generator;

import java.io.PrintStream;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;

import com.github.javaparser.JavaToken;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.body.FieldDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.body.VariableDeclarator;
import com.github.javaparser.ast.expr.AssignExpr;

/**
 * Utilities for parse AST (Abstract Syntax Tree)
 * @author Pierre-Yves Gibello - OW2
 *
 */
public class ASTUtils {
	
	/**
	 * Determine if a modification is required to add an inline JML annotation to a java method declaration
	 * @param declaration The java method declaration to scan
	 * @param jmlAnnotation The JML annotation to add (inline)
	 * @return a Modification object if required (null if the annotation was already there)
	 */
	public static Modification locateInlineAnnotation(MethodDeclaration declaration, String jmlAnnotation) {
		Modification result = null;
		if(declaration.getTokenRange().isPresent()) {
			Iterator<JavaToken> ti = declaration.getTokenRange().get().iterator();
			boolean done = false;
			while(ti.hasNext() && !done) {
				JavaToken tok = ti.next();
				if(tok.getText().contains(jmlAnnotation + " ")) done = true;
				else if(tok.getText().equals(declaration.getNameAsString())) {
					JavaToken begin;
					try {
						begin = declaration.getTokenRange().get().getBegin();
						boolean modifierPresent = true;
						while(! isMethodModifier(begin) && modifierPresent) {
							begin = begin.getNextToken().get();
							modifierPresent = ! begin.getText().equals(declaration.getNameAsString());
						}
						int column = begin.getRange().get().end.column + 1;
						if(! modifierPresent) {
							// Reached method name without modifier
							// like for "int method()" or "static void method()"
							do {
								begin = begin.getPreviousToken().get();
							} while(begin.getText().trim().isEmpty()); // Java11 String.isBlank()*/
							column = Math.max(begin.getRange().get().begin.column - 1, 0);
						}
						if(declaration.findCompilationUnit().isPresent()) {
							result = new Modification(jmlAnnotation,
									true, //Inline annotation
									begin.getRange().get().end.line,
									column,
									declaration.findCompilationUnit().get());
						}
					} catch(NoSuchElementException e) {
						// Type not found (see JavaToken typeTok above)
						System.err.println(declaration);
						e.printStackTrace(System.err);
					}
				}
			}
		}
		return result;
	}
	
	/**
	 * Determine if a modification is required to add a JML annotation to a java method comment
	 * @param declaration The java method declaration to scan
	 * @param jmlAnnotation The JML annotation to add (in method comment)
	 * @return a Modification object if required (null if the annotation was already there)
	 */
	public static Modification locateAnnotation(MethodDeclaration declaration, String jmlAnnotation) {
		if(declaration.getComment().isPresent()
			&& declaration.getComment().get().getContent().contains(jmlAnnotation)) {
				return null;
		} else {
			return new Modification(jmlAnnotation,
					false, //Not inline
					declaration.getRange().get().begin.line,
					declaration.getRange().get().begin.column,
					declaration.findCompilationUnit().get());
		}
	}

	/**
	 * Tells whether a java token is a method modifier
	 * @param tok The java token
	 * @return true if token is a method modifier, false otherwise
	 */
	private static boolean isMethodModifier(JavaToken tok) {
		String text = tok.getText();
		return "public".equals(text) || "protected".equals(text) || "private".equals(text);
	}

	/**
	 * Tells whether a method is a getter (is called "getXXX" where "xXX" is a field)
	 * @param declaration The method declaration
	 * @return true if method is a getter, false otherwise
	 */
	public static boolean isGetter(MethodDeclaration declaration) {
		if(declaration.findCompilationUnit().isPresent()) {
			CompilationUnit cu = declaration.findCompilationUnit().get();
			String methodName = declaration.getNameAsString().trim();
			String fieldName = getFieldFromGetterOrSetter(methodName);
			if(fieldName != null
			  && (methodName.startsWith("get") || methodName.startsWith("is"))) { // Method looks like a getter
				for(FieldDeclaration field : cu.getChildNodesByType(FieldDeclaration.class)) {
					for(VariableDeclarator var : field.getVariables()) {
						if(fieldName.equals(var.getNameAsString())) {
							return true; // Method is a getter!
						}
					}
				}
			}
		}
		return false;
	}

	/**
	 * Retrieves list of all public fields assigned in a given method
	 * @param declaration The method declaration
	 * @return List of public fields assigned in the method (empty list if none)
	 */
	public static List<String> getAssignedPublicFields(MethodDeclaration declaration) {
		List<String> result = new LinkedList<String>();
		
		if(declaration.findCompilationUnit().isPresent()) {
			CompilationUnit cu = declaration.findCompilationUnit().get();					

			for(FieldDeclaration field : cu.getChildNodesByType(FieldDeclaration.class)) {
				for(VariableDeclarator var : field.getVariables()) {
					if(field.isPublic()) { // For all public fields in class...
						// ... look for field assignment in method
						for(AssignExpr assignment : declaration.getChildNodesByType(AssignExpr.class)) {
							String assigned = assignment.getTarget().toString();
							if(assigned.startsWith("this.")) assigned = assigned.substring(5);
							if(assigned.equals(var.getNameAsString())) {
								result.add(assigned); // Public field assigned
							}
						}
					}
				}
			}
		}
		return result;
	}
	
	/**
	 * Extracts field name out of getter/setter method name
	 * @param methodName the method name (either getXXX or setXXX)
	 * @return The field name, null if it can't be guessed
	 */
	public static String getFieldFromGetterOrSetter(String methodName) {
		if((methodName.startsWith("set") || methodName.startsWith("get")) && methodName.length() > 3) {
			String fieldName = methodName.substring(3);
			return fieldName.replace(fieldName.charAt(0), Character.toLowerCase(fieldName.charAt(0)));
		} else if(methodName.startsWith("is") && methodName.length() > 2) {
			String fieldName = methodName.substring(2);
			return fieldName.replace(fieldName.charAt(0), Character.toLowerCase(fieldName.charAt(0)));
		} else return null;
	}

	/**
	 * Prints compilation unit on specified output
	 * @param cu The CU to print
	 * @param out The output PrintStream
	 */
	public static void printCU(CompilationUnit cu, PrintStream out) {
		if(! cu.getTokenRange().isEmpty()) {
			Iterator<JavaToken> tokens = cu.getTokenRange().get().iterator();
			while(tokens.hasNext()) {
				JavaToken tok = tokens.next();
				out.print(tok.getText());
			}
		}
	}
	
	/**
	 * Prints AST on specified output
	 * @param node The node to start from
	 * @param out The output PrintStream
	 */
	public static void printAST(Node node, PrintStream out) {
		printAST(0, node, out);
	}
	
	/**
	 * Prints AST on specified output, with specified tabulation (space count)
	 * @param tab The tabulation
	 * @param node The node to start from
	 * @param out The output PrintStream
	 */
	private static void printAST(int tab, Node node, PrintStream out) {
		for(int i=0; i<tab; i++) out.print(" ");
		out.println(node + "[" + node.getClass() + "]");
		for(Node n: node.getChildNodes()) {
			printAST(tab+1, n, out);
		}
	}
}
