package eu.decoder.jmlgen.generator;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;

import com.github.javaparser.ParseResult;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.PackageDeclaration;
import com.github.javaparser.symbolsolver.JavaSymbolSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.CombinedTypeSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.JavaParserTypeSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.ReflectionTypeSolver;
import com.github.javaparser.utils.SourceRoot;

/**
 * JML generator main class.
 * @author Pierre-Yves Gibello - OW2
 *
 */
public class JmlGenerator {
	
	CombinedTypeSolver solver = new CombinedTypeSolver();
	String projectRoot = "." + File.separator; // projectRoot MUST end with separator char
	List<SourceRoot>sourceRoots = new LinkedList<SourceRoot>();
	HashMap<String, String> cuToPath = new HashMap<String, String>();

	public static void main(String args[]) throws Exception {

		String projectRoot = null, destination = null, sourcepath = "src/main/java";
		
		if(args.length <= 0) {
			// error
			System.err.println("Usage: JmlGenerator <propertiesFile> | JmlGenerator srcDir destDir [sourcepath]");
			System.exit(1);
		} else if(args.length == 1) {
			Properties config = new Properties();
			config.load(new FileInputStream(args[0]));
			projectRoot = config.getProperty("root");
			sourcepath = config.getProperty("sourcepath");
			if(sourcepath == null) sourcepath = "src/main/java";
			destination = config.getProperty("destination");
		} else if(args.length >= 2) {
			projectRoot = args[0].trim();
			destination = args[1].trim();
			if(args.length == 3) sourcepath = args[2].trim();
			else {
				// error
				System.err.println("Usage: JmlGenerator <propertiesFile> | JmlGenerator rootDir destDir [sourcepath]");
				System.exit(1);
			}
		}
		
		if(projectRoot == null || destination == null) {
			// error
			System.err.println("Error: missing project root dir or destination dir");
			System.exit(1);
		}

		(new JmlGenerator()).generateJml(projectRoot, destination, sourcepath, System.out); // Messages on System.out
	}
	
	/**
	 * Generate JML
	 * @param projectRoot Project root directory
	 * @param destination Destination for destination (overwrite if identical to projectRoot)
	 * @param codePath Colon-separated list of paths where to find code
	 * @param log Output stream for logging (null for none)
	 * @return The set of modified file paths
	 * @throws IOException
	 */
	public Set<String> generateJml(String projectRoot, String destination, String codePath, PrintStream log) throws IOException {
		
		// Reset project
		setProjectRoot(projectRoot);
		this.sourceRoots.clear();
		this.cuToPath.clear();
		this.solver = new CombinedTypeSolver();
		this.solver.add(new ReflectionTypeSolver());
		
		StringTokenizer st = new StringTokenizer(codePath, ":");
		while(st.hasMoreTokens()) {
			addSourcepath(st.nextToken(), true);
		}

		return generateJML(destination, log);
	}

	/**
	 * Generate JML annotations in java files
	 * @param destPath Destination java project path
	 * @return The set of modified file paths
	 * @throws IOException
	 */
	private Set<String> generateJML(String destPath, PrintStream log) throws IOException {

		if(log != null) log.println("Generating JML into " + destPath);

		File destination = new File(destPath);
		if(!destination.exists()) destination.mkdirs();
		if(!destination.isDirectory()) throw new IOException(destPath + " should be a directory");

		// List of all parsed results (from all source roots)
		List<ParseResult<CompilationUnit>> parseResults = new LinkedList<>();

		// First pass to parse/gather CUs + catch CU directories
		if(log != null) log.print("Preparing solvers...");
		for(SourceRoot sourceRoot : sourceRoots) {

			// Resolve symbols only within the parsed code
			sourceRoot.getParserConfiguration().setSymbolResolver(new JavaSymbolSolver(solver));

			List<ParseResult<CompilationUnit>> partialResults = sourceRoot.tryToParse();
			for(ParseResult<CompilationUnit> parseResult: partialResults) {
				if(parseResult.isSuccessful()) {
					CompilationUnit cu = parseResult.getResult().get();
					PackageDeclaration pkg = cu.getPackageDeclaration().get();
					if(! pkg.getName().toString().contains("eu.decoder.jmlgen.")) { // Do not parse our own tests or code !
						//System.out.println(getCUUniqueName(cu) + "->" + getCURelativeDir(cu, sourceRoot.getRoot().toFile().toString() + File.separator));
						cuToPath.put(getCUUniqueName(cu), getCURelativeDir(cu, sourceRoot.getRoot().toFile().toString() + File.separator));
					}
				}
			}
			
			parseResults.addAll(partialResults);
		}
		if(log != null) log.println("...done!");

		FileUtils fileUtils = new FileUtils();

		// 3rd pass to generate JML @pure
		if(log != null) log.print("Try to generate assertions @pure for getters and @modifies for methods affecting public fields...");
		for(ParseResult<CompilationUnit> parseResult: parseResults) {
			if(parseResult.isSuccessful()) {
				CompilationUnit cu = parseResult.getResult().get();
				PackageDeclaration pkg = cu.getPackageDeclaration().get();
				if(! pkg.getName().toString().contains("eu.decoder.jmlgen") || pkg.getName().toString().endsWith(".sample")) {
					//System.out.println(cu.getPrimaryTypeName());
					JmlPureInsertionVisitor jmlInsertionVisitor = new JmlPureInsertionVisitor(this, fileUtils, destination);
					jmlInsertionVisitor.visit(cu, null);
				}
			}
		}
		if(log != null) log.println("...done!");

		// 2nd pass to generate JML @non_null
		if(log != null) log.print("Try to generate @non_null assertions for methods at risk of NPE...");
		for(ParseResult<CompilationUnit> parseResult: parseResults) {
			if(parseResult.isSuccessful()) {
				CompilationUnit cu = parseResult.getResult().get();
				PackageDeclaration pkg = cu.getPackageDeclaration().get();
				if(! pkg.getName().toString().contains("eu.decoder.jmlgen") || pkg.getName().toString().endsWith(".sample")) {
					//System.out.println(cu.getPrimaryTypeName());
					JmlNonNullInsertionVisitor jmlInsertionVisitor = new JmlNonNullInsertionVisitor(this, fileUtils, destination);
					jmlInsertionVisitor.visit(cu, null);
				}
			}
		}
		if(log != null) log.println("...done!");

		if(log != null) fileUtils.printModifications(log);
		for(String modifiedRelPath : fileUtils.getModifications().keySet()) {
			File target = FileUtils.copyFile(new File(projectRoot), new File(destPath), modifiedRelPath);
			fileUtils.applyModifications(target, fileUtils.getModifications().get(modifiedRelPath));
		}
		
		if(log != null) log.println("JML generation terminated.");

		return fileUtils.getModifications().keySet();
	}

	/**
	 * Computes a unique identifier for a given compilation unit.
	 * @param cu The compilation unit
	 * @return the unique identifier of the specified CU
	 */
	private String getCUUniqueName(CompilationUnit cu) {
		return cu.getPackageDeclaration().get().getNameAsString() + "." + cu.getPrimaryTypeName().get();
	}

	/**
	 * Retrieves the directory a compilation unit belongs to, relative to project root
	 * @param cu The compilation unit
	 * @param srcPath The source path declared for this compilation unit
	 * @return Path to compilation unit directory, relative to project root path
	 */
	private String getCURelativeDir(CompilationUnit cu, String srcPath) {
		if(srcPath == null) srcPath = "";
		if(srcPath.startsWith(this.projectRoot)) {
			srcPath = srcPath.substring(this.projectRoot.length());
			if(! srcPath.endsWith(File.separator)) srcPath += File.separator;
		}
		if(cu.getPrimaryTypeName().isPresent()) {
			return srcPath
					+ (cu.getPackageDeclaration().isPresent() ?
						cu.getPackageDeclaration().get().getNameAsString().replace(".", File.separator) : "");
		} else {
			return null;
		}
	}
	
	public String getCUPath(CompilationUnit cu) {
		return this.cuToPath.get(getCUUniqueName(cu)) + File.separator + cu.getPrimaryTypeName().get() + ".java";
	}

	/**
	 * Set java project root
	 * @param path The project root
	 */
	private void setProjectRoot(String path) {
		if(path != null) {
			path = path.trim();
			this.projectRoot = (path.endsWith(File.separator) ? path : path + File.separator);
		}
	}

	/**
	 * Add path to java (root of java packages, like <projectRoot>/src/main/java
	 * @param path Path to add (only paths relative to projectRoot allowed if parseAndResolve is true).
	 * @param parseAndResolve If false, path is used to resolve only. If true, files will be parsed as well.
	 * @throws IOException
	 */
	private void addSourcepath(String path, boolean parseAndResolve) throws IOException {
		File f = new File(path);
		SourceRoot sourceRoot;
		if(f.isAbsolute()) {
			if(parseAndResolve) throw new IOException(path + ": Only paths relative to project root can be parsed (set parseAndResolve to false)");
			sourceRoot = new SourceRoot(f.toPath());
		} else {
			sourceRoot = new SourceRoot((new File(this.projectRoot + path)).toPath());
		}
		this.solver.add(new JavaParserTypeSolver(sourceRoot.getRoot()));
		if(parseAndResolve) this.sourceRoots.add(sourceRoot);
	}
}

